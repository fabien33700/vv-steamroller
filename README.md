#Projet vv-steamroller

**Alex Siharath** & **Fabien Le Houëdec**

##Présentation du projet

Le but de ce projet est de tester les algorithmes de compression et de décompression Lempel-Ziv-Welch et du codage de Huffman en Java.

Ce projet est divisé en 2 parties pour chacun des deux algorithmes. 
Les entrées pour chacun de ses programmes sont de type `InputStream`.
Le comportement standard d'un algorithme est décrit par l'interface `Algorithm` qui définit les méthodes `compress` et `decompress`

##Implémentation de [Lempel-Ziv-Welch](src/main/java/fr/istic/steam/roller/compress/algo/LZW.java)

Les algorithmes ont été implémentés en suivant [Lempel-Ziv-Welch](https://fr.wikipedia.org/wiki/Lempel-Ziv-Welch). Nous avons eu des problèmes dans l'implémentation de la fonction de compression du fichier, 
en suivant seulement l'encodage proposé par [Lempel-Ziv-Welch](https://fr.wikipedia.org/wiki/Lempel-Ziv-Welch) l'écriture rendue par l'algorithme n'était pas la même que dans le fichier,
Nous avons dû pour cela ajouter une étape d'écriture dans le fichier après l'encodage pour écrire dans le fichier correctement.

###Test unitaire [Lempel-Ziv-Welch](src/test/java/fr/istic/steam/roller/compress/algo/LZWTest.java)

Afin de voir si notre code de compression et de décompression fonctionne correctement, nous avons testé, à l'aide de test unitaires, qu'après chaque compression : 
* le fichier lzw produit n'est pas vide
* le fichier lzw produit et un autre fichier lzw crée précédemment sont identiques
* le fichier lzw produit et un autre fichier lzw crée précédemment ont la même taille
* le dictionnaire est identique à celui attendu
* la taille du fichier lzw produit était inférieure à celle du fichier d'origine

De même pour la décompression, nous avons écrit les tests permettant de vérifier que :  
* le fichier texte produit n'est pas vide
* le fichier texte produit et le fichier texte d'origine sont identiques
* le fichier texte produit et le fichier texte d'origine ont la même taille
* le dictionnaire de la décompression était le même que celui attendu
* la taille du fichier texte produit était supérieur à celle du fichier lzw


Ces tests ont été effectués sur deux fichiers : 
 - un petit [wiki.txt](src/test/resources/text/wiki.txt) 
 - un plus gros [bigWiki.txt](src/test/resources/text/bigWiki.txt)

###Résultats tests unitaire [Lempel-Ziv-Welch](src/test/java/fr/istic/steam/roller/compress/algo/LZWTest.java)

Les tests passent parfaitement sur les deux exemples ci-dessus, mais nous avons rencontré des cas non passants, notamment : 
  - des problèmes liés à l'encodage (caractères spéciaux hors plage ASCII) => NullPointerException
  - taille de fichier importante :  la compression s'arrête prématurément
  - si le fichier ne contient pas assez répétition la compression produit un fichier plus gros que l'original

Pour les premiers cas nous n'avons pas trouvé de façon pour la résoudre et le deuxième cas c'est à cause de l'algorithme qui de base ne fait pas de perte.
  
###Test unitaire [Huffman](src/test/java/fr/istic/steam/roller/compress/algo/HuffmanTest.java)

Compression : 
* le fichier compressé avec Huffman a la taille attendue
* compresser un fichier vide doit donner un fichier vide
* le fichier compressé avec Huffman est identique à l'attendu(*)
* la table d'encodage est la même que celle attendue(*)
* l'arbre de Huffmann est le même qu'attendu(*)
* la taille du fichier compressé est inférieure à celle du fichier d'origine
* le fichier compressé doit avoir le bon en-tête

Décompression
* la décompression doit échouer sur un fichier vide
* la décompression doit échouer sur un fichier ne possédant pas le bon en-tête
* décompresser un fichier compressé avec le même algorithme doit donner un fichier identique à la source
* la taille du fichier texte décompressé doit être supérieur à celle du fichier compressé

(*) Sur le fichier shortText.txt, j'ai appliqué l'algorithme sur papier avec une courte séquence de caractère "*AAAAAABCCCCCCDDEEEEE*", ce qui m'a permit d'avoir un attendu précis pour les tests avec cette séquence en entrée.

 - JimFs
 
 Pour rendre les tests moins dépendants à la plateforme et au système de fichiers, j'ai utilisé **JimFS** qui est un système de fichier virtuel pour Java. Il permet de monter une arborescence virtuelle qui peut être rétablie à son état initial : pour cela, j'ai créé une méthode `mountMockedFs()` annotée avec `@BeforeEach` : cela permet de créer l'arborescence et de copier les fichiers en entrée depuis les ressources du projet, avant d'exécuter chaque test. La limitation est qu'il faut nécessairement passer par les classes de manipulation de fichiers `Paths` et `Files` de l'API **NIO** de Java 7.
 Les appels traditionnels aux classes `File` et `FileInputStream` ne fonctionnent pas, il faut donc adapter un peu le code de la classe à tester.
 
 - Mockito
 
 La classe `Compressor` contient l'interpréteur de ligne de commande pour notre application : elle analyse les arguments passés en entrées pour initialiser la configuration de la (dé)compression. Elle fait ensuite appel à l'implémentation d'`Algorithm` adéquate en fonction de celui sélectionné par l'utilisateur (argument `-a|--algorithm`) et en lui passant les instances d'`InputStream` et `OutputStream` correspondant aux fichiers spécifiés en arguments.
 
 Pour tester cette classe, j'ai voulu m'assurer qu'elle appelait bien les méthodes `compress()` et `decompress()` de l'interface Algorithm. Pour conserver une bonne isolation du code, j'ai utilisé **Mockito** pour créer un mock d'`Algorithm` de manière déclarative, pour vérifier que `Compressor` appelait bien les méthodes adéquates. Cela m'a dispenser de créer une implémentation de test.
  
###Résultats tests unitaire [Huffman](src/test/java/fr/istic/steam/roller/compress/algo/HuffmanTest.java)

Tous les tests sont passants, hormis 2 : 
  - `compress_FileMustHaveTheExpectedSize()`
  - `compress_FileMustBeAsExpected()`
  
 dans le cas où le fichier d'entrée est `huffmanWiki.txt`. Cela est dû à une différence de contenu entre le produit et l'attendu (de l'ordre de quelques dizaine d'octets). Je n'ai pas poursuivi l'analyse car cela aurait pris beaucoup de temps mais soit l'algorithme dérive avec les gros fichiers, soit les caractères accentuées altèrent la compression.

###Outils de tests

Pour exécuter tous les outils, lancer la commande `./mvnw verify` ou `mvn verify` dans un terminal

####Jacoco

Nous avons utilisé ce plugin pour obtenir le taux de couverture de code de nos tests. Voici un extrait de résultat de son exécution : 
![Jacoco](img/Jacoco.png)

Pour ouvrir le HTML du rapport, exécuter : **xdg-open target/site/jacoco/index.html**

####PMD
Pour analyser notre code, nous avons utilisé PMD qui décrit un certain nombre de règles pour renforcer la qualité de code : ils détectent
des problèmes comme le code mort, les imbrications trop complexes, etc...

Voici un extrait du résultat obtenu :

![PMD](img/PMD.png)

Cette analyse nous détaille par ordre de priorité les erreurs rencontrées dans le code, dans notre cas nous avons beaucoup d'erreurs majeures.

Pour le lancer, exécuter la commande `mvn pmd:check`

Pour ouvrir le HTML du rapport, exécuter : **xdg-open target/site/pmd.html** (s'il y a des erreurs)

#####Correction des erreurs trouvées par PMD
Les erreurs sont très simples ici à corriger nous avons juste des parenthèses inutiles, donc une modification direct suffit.

####SpotBugs
Pour l'analyse de bugs, nous avons aussi utilisé Spotbugs qui est un analyseur statique de Bytecode.
On peut le lancer seul via `mvn compile spotbugs:check`

Pour ouvrir le HTML du rapport, exécuter : **xdg-open target/site/spotbugs.xml** (s'il y a des erreurs) ou **mvn spotbugs:gui** pour avoir une interface.
 
Un petit aperçu de l'interface :
 
![SpotBugs](img/spotbugs.png)

#####Correction des bugs trouvés par SpotBugs
Nous avons comme erreurs :
- A parameter is dead upon entry to a method but overwritten
    - Pour ce problème nous n'avons qu'à, soit changer de fichier ou créer un fichier de ce nom
    
- Potential Path Traversal
    - La remarque est que nous utilisons directement les chemins vers les fichiers pour nos tests, 
      SpotBugs nous dit que s'il les fichiers ne sont pas à cette place, nous aurons des erreurs, nous ne savons pas vraiment comment résoudre ce problème.
      l'autre problème est que nous n'avons pas fermé les stream, c'est très facile de le faire.

- Method may fail to clean up stream or resource
    - Pour celui-là il nous a seulement fallu fermer les stream.
     
- Potential CRLF Injection for logs
    - Ici le problème est que la data donnée n'est pas sûr donc pour y remédier on pourrait juste changer le log rendu par un simple string personnalisé mais le message ne sera plus précis, SpotBugs nous propose une notre solution qui est de faire [`log.info("User " + val.replaceAll("[\r\n]","") + " (" + userAgent.replaceAll("[\r\n]","") + ")`](code) mais n'avons pas réussi à le mettre en place.

- URLConnection Server-Side Request Forgery (SSRF) and File Disclosure
    - le problème ici est que nous devons pas faire confiance au user qui appelle à l'API, SpotBugs nous propose plusieurs solutions :
        - Accepter une clé de destination, et l'utiliser pour rechercher la destination cible associée à la clé
        - Utiliser une white list
    Malheureseument nous avons pas réussi à appliquer les solutions.
 
- Consider using Locale parameterized version of invoked method
    - Ici non plus nous n'avons pas trouver de solution étant un bug mineur la plupart des gens supprime le warning
 

####Plugins intellij QAPlug with PMD, FindBugs, Checkstyle
Ce plugin permet de condenser les données issues des rapports de ces trois outils pour y accéder directement dans IntelliJ.
En voici un exemple : 

![QAplug](img/Findbug%20and%20PMD%20after%20frist%20installation.png)

###Conclusion des outils d'analyse de code
On peut voir que notre code contient beaucoup d' "erreurs" et de "problèmes", mais ces outils nous permettent de les repérer plus facilement, et de nous indiquer précisément leur nature
pour pouvoir leur attribuer une priorité et rapidement mener les actions correctives visant à améliorer la qualité du code.

##Mutation testing avec Pitest
Nous aurions pu faire la mutation du code manuellement mais pour plus d'efficacité, nous avons décidé d'exécuter le *mutation testing* avec des outils automatisant le processus.
Ici nous avons utilisé **Pitest** car contrairement à **Javalanche** il se présente sous la forme d'un plugin Maven.
Après analyse, nous obtenons un rapport au format HTML :
![mutation](img/mutation.png)

Ces rapports sont situés dans [target/pit-reports/YYYYMMDDHHMI](target/pit-reports)

Dans le cas de cette exemple LZW sur les 37 mutations 30 ont été tuées, ce qui veut signifie que sur les mutations 30, nos tests ne sont pas passé, mais 7 codes mutés sont passé.

###Analyse du mutation testing

Le but de ce test est que tous les codes mutés ne passe pas les tests, dans notre cas ce n'est pas le cas, nous avons remarquer que les cas où les codes mutés passent,
sont dans le cas où la ligne de code n'impacte pas du tout le but de la fonction comme par exemple, le cas où la mutation est d'enlever le retour du catch ou encore les .close() des stream qui en soit n'impactent en rien la fonction.

###Gitlab CI

Comme demandé, nous avons mis en place une chaîne d'intégration continue pour l'exécution automatisée de nos tests.
Nous avons utilisé Gitlab CI, fourni par le site *Gitlab.com*.
L'avantage de cette solution est de pouvoir disposer rapidement d'une solution prête à l'emploi, avec un pool de runners gratuit.
Il nous a juste fallu déclarer un fichier `.gitlab-ci.yml` pour configurer les jobs à exécuter à chaque push de nos travaux sur la branche `master`.

Ce fichier décrit la configuration de l'exécution du build. Nous utilisons une image Java 8 + Maven3, dans laquelle nous définissons la commande à exécuter pour lancer le build, à savoir `mvn test`.

Sur cette capture, on peut voir la liste des jobs de builds déjà lancés, avec l'état de l'exécution (Passed / Failed) :

![Gitlab CI Jobs](img/gitlab-ci1.png)

Sur cette autre capture : on peut voir la sortie standard de l'exécution d'un job sur un runner :

![Gitlab CI Build output](img/gitlab-ci2.png)

##Conclusion

Finalement lors de nos tests unitaire nous avons un taux de coverage de 65% sur la totalité du projet et 98% sur nos classes où nous avons tous nos algorithme, avec les outils d'analyse PMD et SpotBugs nous avons trouvé que notre code possède beaucoup d'erreurs de plus ou moins fortes gravité, la plupart on peut être corrigé surtout ce de PMD mais SpotBugs nous a montré des erreurs que nous n'avons pas réussi à régler.

Concernant le mutation testing, nous avons un coverage de 96%, 97 mutations ont été généré et 86 sont morts, après une rapide analyse, il est montré que certaines mutations touchent le contenu de [`catch(IOException exception){...}`](#code) et catch qui est obligatoire pour l'utilisation des DataInputStream mais dont l'erreur est attrapé bien avant par le InputStream, donc le changement de ces ligne n'ont aucun impact sur nos algorithme et sur notre code en général.

Ce projet nous a appris à utiliser les outils de test et d'analyse, bien que nous les avions déjà utiliser, beaucoup de lacunes sont présentes.
Nous avons aussi remarqué que le travail de test efficace peut être très long, ici nous n'avons pas trouvé de solution pour résoudre les bugs rapportés de SpotBugs.
De ce fait, notre outil CI ne nous fait pas passer les tests à cause des erreurs SpotBugs comme cité plus tôt. Pour pouvoir les faire passer nous avons exclus SpotBugs lors du test il sera possible de le lancement à part voir la section à ce sujet.

