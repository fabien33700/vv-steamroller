package fr.istic.steam.roller.compress.algo;

import org.apache.commons.io.FileUtils;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.*;

class LZWTest {

    private InputStream in;
    private OutputStream out;
    private int NUMBER_OF_ASCII_CHARS = 256;
    private LZW lzwTest = new LZW();
    private String PATH_RESSOURCES_LZW = "src/test/resources/lzw/";
    private String PATH_RESSOURCES_TEXT = "src/test/resources/text/";

    @Test
    void compress() throws IOException {
        writeWikiLZWtFile();
        in = new FileInputStream(new File(PATH_RESSOURCES_TEXT+"wiki.txt"));
        out = new FileOutputStream(new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw"));
        lzwTest.compress(in,out);
        File current = new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw");
        File expected = new File(PATH_RESSOURCES_LZW+"wikiTestFileLZW.lzw");
        assertTrue(FileUtils.contentEquals(current, expected));
    }

    @Test
    void getEncodeDictionary() throws IOException {
        in = new FileInputStream(new File(PATH_RESSOURCES_TEXT+"wiki.txt"));
        out = new FileOutputStream(new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw"));
        lzwTest.compress(in,out);

        Map<String, Integer> actual = lzwTest.getEncodeDictionary();

        Map<String, Integer> expected = new HashMap<>();
        for (int i = 0; i < NUMBER_OF_ASCII_CHARS; i++) {
            expected.put("" + (char) i, i);
        }
        expected.put("TO", 256);
        expected.put("OB", 257);
        expected.put("BE", 258);
        expected.put("EO", 259);
        expected.put("OR", 260);
        expected.put("RN", 261);
        expected.put("NO", 262);
        expected.put("OT", 263);
        expected.put("TT", 264);
        expected.put("TOB", 265);
        expected.put("BEO", 266);
        expected.put("ORT", 267);
        expected.put("TOBE", 268);
        expected.put("EOR", 269);
        expected.put("RNO", 270);
        assertThat(actual, is(expected));
    }

    @Test
    void compressWikiCompareEqualFileLZW() throws IOException {
        writeWikiLZWtFile();
        in = new FileInputStream(new File(PATH_RESSOURCES_TEXT+"wiki.txt"));
        out = new FileOutputStream(new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw"));
        lzwTest.compress(in,out);
        File current = new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw");
        File expected = new File(PATH_RESSOURCES_LZW+"wikiTestFileLZW.lzw");
        assertTrue(FileUtils.contentEquals(current, expected));
    }

    @Test
    void compressWikiCompareIsEmptyFileLZW() throws IOException{
        in = new FileInputStream(new File(PATH_RESSOURCES_TEXT+"wiki.txt"));
        out = new FileOutputStream(new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw"));
        lzwTest.compress(in,out);
        File current = new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw");
        assertNotNull(current);
    }

    @Test
    void compressWikiCompareEqualLengthFileLZW() throws IOException{
        writeWikiLZWtFile();
        in = new FileInputStream(new File(PATH_RESSOURCES_TEXT+"wiki.txt"));
        out = new FileOutputStream(new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw"));
        lzwTest.compress(in,out);
        File current = new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw");
        File expected = new File(PATH_RESSOURCES_LZW+"wikiTestFileLZW.lzw");
        assertEquals(current.length(), expected.length());
    }

    @Test
    void compressBigWikiLZWFileSmallerThanText() throws IOException {
        File textFile = new File(PATH_RESSOURCES_TEXT+"bigWiki.txt");
        in = new FileInputStream(textFile);
        out = new FileOutputStream(new File(PATH_RESSOURCES_LZW+"bigWikiEncode.lzw"));
        lzwTest.compress(in,out);
        File lzwFile = new File(PATH_RESSOURCES_LZW+"bigWikiEncode.lzw");
        assertTrue(textFile.length()>lzwFile.length());
    }

    @Test
    void decompressWikiIsNotNull() throws IOException{
        in = new FileInputStream(new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw"));
        out = new FileOutputStream(new File(PATH_RESSOURCES_TEXT+"wikiDecode.txt"));
        lzwTest.decompress(in, out);
        File txtFile = new File(PATH_RESSOURCES_TEXT+"wikiDecode.txt");
        assertNotNull(txtFile);
    }

    @Test
    void decompress() throws IOException{
        in = new FileInputStream(new File(PATH_RESSOURCES_TEXT+"wiki.txt"));
        out = new FileOutputStream(new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw"));
        lzwTest.compress(in,out);
        in = new FileInputStream(new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw"));
        out = new FileOutputStream(new File(PATH_RESSOURCES_TEXT+"wikiDecode.txt"));
        lzwTest.decompress(in, out);
        File current = new File(PATH_RESSOURCES_TEXT+"wikiDecode.txt");
        File expected = new File(PATH_RESSOURCES_TEXT+"wiki.txt");
        assertTrue(FileUtils.contentEquals(current, expected));
    }

    @Test
    void decompressBigWikiEqualOriginalText() throws IOException{
        in = new FileInputStream(PATH_RESSOURCES_TEXT+"bigWiki.txt");
        out = new FileOutputStream(new File(PATH_RESSOURCES_LZW+"bigWikiEncode.lzw"));
        lzwTest.compress(in, out);
        in = new FileInputStream(new File(PATH_RESSOURCES_LZW+"bigWikiEncode.lzw"));
        out = new FileOutputStream(new File(PATH_RESSOURCES_TEXT+"bigWikiDecode.txt"));
        lzwTest.decompress(in, out);
        File current = new File(PATH_RESSOURCES_TEXT+"bigWikiDecode.txt");
        File expected = new File(PATH_RESSOURCES_TEXT+"bigWiki.txt");
        assertTrue(FileUtils.contentEquals(current, expected));
    }

    @Test
    void decompressBigWikibiggerthanLZW() throws IOException{
        in = new FileInputStream(PATH_RESSOURCES_TEXT+"bigWiki.txt");
        out = new FileOutputStream(new File(PATH_RESSOURCES_LZW+"bigWikiEncode.lzw"));
        lzwTest.compress(in, out);
        in = new FileInputStream(new File(PATH_RESSOURCES_LZW+"bigWikiEncode.lzw"));
        out = new FileOutputStream(new File(PATH_RESSOURCES_TEXT+"bigWikiDecode.txt"));
        lzwTest.decompress(in, out);
        File txtFile = new File(PATH_RESSOURCES_TEXT+"bigWikiDecode.txt");
        File lzwFile = new File(PATH_RESSOURCES_LZW+"bigWikiEncode.lzw");
        assertTrue(txtFile.length() > lzwFile.length());
    }

    @Test
    void decompressWikibiggerthanLZW() throws IOException{
        in = new FileInputStream(new File(PATH_RESSOURCES_TEXT+"wiki.txt"));
        out = new FileOutputStream(new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw"));
        lzwTest.compress(in,out);
        in = new FileInputStream(new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw"));
        out = new FileOutputStream(new File(PATH_RESSOURCES_TEXT+"wikiDecode.txt"));
        lzwTest.decompress(in, out);
        File txtFile = new File(PATH_RESSOURCES_TEXT+"wikiDecode.txt");
        File lzwFile = new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw");
        assertFalse(txtFile.length() > lzwFile.length());
    }

    @Test
    void getDecodeDictionary() throws IOException {
        in = new FileInputStream(new File(PATH_RESSOURCES_LZW+"wikiEncode.lzw"));
        out = new FileOutputStream(new File(PATH_RESSOURCES_TEXT+"wiki.txt"));
        lzwTest.decompress(in,out);

        Map<Integer, String> actual = lzwTest.getDecodeDictionary();

        Map<Integer, String> expected = new HashMap<>();
        for (int i = 0; i < NUMBER_OF_ASCII_CHARS; i++) {
            expected.put(i, "" + (char) i);
        }
        expected.put( 256,"TO");
        expected.put( 257,"OB");
        expected.put( 258,"BE");
        expected.put( 259,"EO");
        expected.put( 260,"OR");
        expected.put( 261,"RN");
        expected.put( 262,"NO");
        expected.put( 263,"OT");
        expected.put( 264,"TT");
        expected.put( 265,"TOB");
        expected.put( 266,"BEO");
        expected.put( 267,"ORT");
        expected.put( 268,"TOBE");
        expected.put( 269,"EOR");
        expected.put( 270,"RNO");
        assertThat(actual, is(expected));
    }

    @Test
    void getNUMBER_OF_ASCII_CHARS() {
        assertEquals(256, lzwTest.getNUMBER_OF_ASCII_CHARS());
    }

    private void writeWikiLZWtFile() throws IOException {
        OutputStream outputStream = new FileOutputStream(new File(PATH_RESSOURCES_LZW+"wikiTestFileLZW.lzw"));
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
        byte[] tmpByte = {0, 84, 0, 79, 0, 66, 0, 69, 0, 79, 0, 82, 0, 78, 0, 79, 0, 84, 1, 0, 1, 2, 1, 4, 1, 9, 1, 3, 1, 5, 1, 7};
        dataOutputStream.write(tmpByte, 0, tmpByte.length);
        dataOutputStream.close();
        outputStream.close();
    }

    @Test
    void headers_MustBeBit1AndBit0() {
        boolean[] headers = lzwTest.header();

        assertThat(headers, Matchers.is( not( nullValue() ) ));
        assertThat(headers.length, Matchers.is(2));
        assertThat(headers[0], Matchers.is(true));
        assertThat(headers[1], Matchers.is(false));
    }
}