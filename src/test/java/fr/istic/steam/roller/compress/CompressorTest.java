package fr.istic.steam.roller.compress;

import fr.istic.steam.roller.compress.algo.Algorithm;
import fr.istic.steam.roller.compress.algo.Huffman;
import fr.istic.steam.roller.compress.algo.LZW;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThrows;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

class CompressorTest {
    Compressor comp;

    private static String[] varargs(String... args) {
        return args;
    }

    @AfterEach
    void tearDown() {
        try {
            Files.deleteIfExists(Paths.get("files/dest"));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        comp = null;
    }

    @Test
    void create_errorWhileArgsParsing() {
        // No args
        assertThrows(IllegalArgumentException.class, () ->
                Compressor.create(varargs("")));

        // No algorithm arg
        assertThrows(IllegalArgumentException.class, () ->
                Compressor.create(varargs("file1.txt", "-o", "file2.txt")));

        // Arg "a" with no value
        assertThrows(IllegalArgumentException.class, () ->
                Compressor.create(varargs("file1.txt", "-a")));

        // No input file specified
        assertThrows(IllegalArgumentException.class, () ->
                Compressor.create(varargs("-a", "huffman")));

        // Invalid algorithm provided
        assertThrows(IllegalArgumentException.class, () ->
                Compressor.create(varargs("file1.txt", "-a", "unknown")));
    }

    @Test
    void create_resultingCompressorObject() {
        // Minimal : default value
        comp = Compressor.create(varargs("file1.txt", "-a", "huffman"));
        assertThat(comp.isCompress(), is(true));
        assertThat(comp.getAlgorithm(), notNullValue());
        assertThat(comp.getAlgorithm(), instanceOf(Huffman.class));
        assertThat(comp.getInFile(), is ("file1.txt"));
        assertThat(comp.getOutFile(), is ("file1.txt.huffman"));

        // Output file
        comp = Compressor.create(varargs("file1.txt", "-a", "lzw", "-o", "file1_comp.txt"));
        assertThat(comp.isCompress(), is(true));
        assertThat(comp.getAlgorithm(), notNullValue());
        assertThat(comp.getAlgorithm(), instanceOf(LZW.class));
        assertThat(comp.getInFile(), is ("file1.txt"));
        assertThat(comp.getOutFile(), is ("file1_comp.txt"));

        // Decompress argument
        comp = Compressor.create(varargs("file1.txt.lzw", "-d",  "-a", "lzw"));
        assertThat(comp.isCompress(), is(false));
        assertThat(comp.getAlgorithm(), notNullValue());
        assertThat(comp.getAlgorithm(), instanceOf(LZW.class));
        assertThat(comp.getInFile(), is ("file1.txt.lzw"));
        assertThat(comp.getOutFile(), is ("file1.txt"));
    }

    @Test
    void execute_MustFailIfInputFileDoesNotExist() {
        // Minimal : default value
        comp = Compressor.create(varargs("unknown.txt", "-a", "huffman"));
        assertThrows(UncheckedIOException.class, comp::execute);
    }

    @Test
    void execute_ProperlyCallAlgorithmProcessingMethods() throws IOException {
        Algorithm mockAlgo = mock(Algorithm.class);
        comp = new Compressor(true, mockAlgo, "file1.txt", "dest");
        comp.execute();

        verify(mockAlgo, times(1)).compress(any(), any());
        Files.delete(Paths.get("files/dest"));

        comp = new Compressor(false, mockAlgo, "file1.txt", "dest");
        comp.execute();
        verify(mockAlgo, times(1)).decompress(any(), any());
    }

    @Test
    void execute_MustRaiseExceptionThrownInProcessingMethod() throws IOException {
        Algorithm mockAlgo = mock(Algorithm.class);
        doThrow(IOException.class).when(mockAlgo).compress(any(), any());
        comp = new Compressor(true, mockAlgo, "file1.txt", "dest");
        assertThrows(UncheckedIOException.class, comp::execute);

        doThrow(IOException.class).when(mockAlgo).decompress(any(), any());
        comp = new Compressor(false, mockAlgo, "file1.txt", "dest");
        assertThrows(UncheckedIOException.class, comp::execute);
    }
}