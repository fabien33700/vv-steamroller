package fr.istic.steam.roller.compress.algo;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;

import static java.nio.file.Files.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

class HuffmanTest {

    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final String[] INPUT_FILES = new String[] { "huffmanWiki.txt", "shortText.txt", "empty.txt" };

    InputStream in;
    OutputStream out;

    Huffman huffman;
    FileSystem fs; // Mocked file system

    @BeforeEach
    void setUp() {
        fs = Jimfs.newFileSystem(Configuration.unix());
        mountMockedFs();
        huffman = new Huffman();
    }

    void mountMockedFs() {
        try {
            createDirectory(fs.getPath("/res"));
            createDirectory(fs.getPath("/text"));

            for (String file : INPUT_FILES) {
                Path dest = fs.getPath("/text", file);
                Files.copy(Paths.get(RESOURCES_PATH, "text", file), dest);
            }

        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    @AfterEach
    void tearDown()  {
        try {
            fs.close();
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
        fs = null;
    }

    private void loadInputOutput(String inputPath, String outputPath) throws IOException {
        in = newInputStream(fs.getPath(inputPath));
        out = newOutputStream(fs.getPath(outputPath));
    }

    @Test
    void compress_FileMustHaveTheExpectedSize() throws Exception {
        long length;

//        loadInputOutput("/text/huffmanWiki.txt", "/res/huffmanWiki.huffman");
//        huffman.compress(in, out);
//        long length = readAllBytes(fs.getPath("/res/huffmanWiki.huffman")).length;
//        assertThat(length, is(equalTo(5625L)));

        loadInputOutput("/text/shortText.txt", "/res/shortText.huffman");
        huffman.compress(in, out);
        length = readAllBytes(fs.getPath("/res/shortText.huffman")).length;
        assertThat(length, is(equalTo(16L)));
    }

    @Test
    void compress_EmptyFileMustGiveEmptyFile() throws Exception {
        in = new ByteArrayInputStream(new byte[0]);
        out = newOutputStream(fs.getPath("/res/empty.huffman"));

        huffman.compress(in, out);
        long length = readAllBytes(fs.getPath("/res/empty.huffman")).length;
        assertThat(length, is(equalTo(0L)));
    }


    @Test
    void compress_FileMustBeAsExpected() throws Exception {
        byte[] result;
        byte[] expected;

//        loadInputOutput("/text/huffmanWiki.txt", "/res/huffmanWiki.huffman");
//        huffman.compress(in, out);
//        byte[] result = Files.readAllBytes(fs.getPath("/res/huffmanWiki.huffman"));
//        byte[] expected = Files.readAllBytes(Paths.get(RESOURCES_PATH, "huffman/huffmanWiki.huffman"));
//        assertThat(Arrays.equals(result, expected), is (true));

        loadInputOutput("/text/shortText.txt", "/res/shortText.huffman");
        huffman.compress(in, out);
        result = Files.readAllBytes(fs.getPath("/res/shortText.huffman"));
        expected = Files.readAllBytes(Paths.get(RESOURCES_PATH, "huffman/shortText.huffman"));
        assertThat(Arrays.equals(result, expected), is(true));
    }

    @Test
    void getFrequencies_ExpectedFrequencies() throws Exception {
        // AAAAAABCCCCCCDDEEEEE
        in = new FileInputStream(RESOURCES_PATH + "/text/shortText.txt");
        byte[] inputBytes = Huffman.toByteArray(in);
        Map<Byte, Integer> frequencies = Huffman.getFrequencies(inputBytes);

        assertThat(frequencies.size(), is(equalTo(5)));
        assertThat(frequencies.keySet(), containsInAnyOrder((byte)65, (byte)66, (byte)67, (byte)68, (byte)69));
        assertThat(frequencies.get((byte) 65), is(6));
        assertThat(frequencies.get((byte) 66), is(1));
        assertThat(frequencies.get((byte) 67), is(6));
        assertThat(frequencies.get((byte) 68), is(2));
        assertThat(frequencies.get((byte) 69), is(5));
    }

    @Test
    void getCodingTable_ExpectedCodingTable() throws Exception {
        // AAAAAABCCCCCCDDEEEEE
        in = new FileInputStream(RESOURCES_PATH + "/text/shortText.txt");
        byte[] inputBytes = Huffman.toByteArray(in);
        Map<Byte, Integer> frequencies = Huffman.getFrequencies(inputBytes);

        Huffman.Node tree = Huffman.buildTree(frequencies);
        Map<Byte, String> codingTable = Huffman.getCodingTable(tree);

        assertThat(codingTable.size(), is(equalTo(5)));
        assertThat(codingTable.keySet(), containsInAnyOrder((byte)65, (byte)66, (byte)67, (byte)68, (byte)69));
        assertThat(codingTable.get((byte) 65), is("10"));
        assertThat(codingTable.get((byte) 66), is("000"));
        assertThat(codingTable.get((byte) 67), is("11"));
        assertThat(codingTable.get((byte) 68), is("001"));
        assertThat(codingTable.get((byte) 69), is("01"));
    }

    @Test
    void getCodingTable_ExpectedHuffmanTree() throws Exception {
        // AAAAAABCCCCCCDDEEEEE
        in = new FileInputStream(RESOURCES_PATH + "/text/shortText.txt");
        byte[] inputBytes = Huffman.toByteArray(in);
        Map<Byte, Integer> frequencies = Huffman.getFrequencies(inputBytes);

        Huffman.Node root = Huffman.buildTree(frequencies);

        assertThat(root, is(not(nullValue())) );
        assertThat(root.freq, is(20));
        assertThat(root.value, is((byte) 0));
        assertThat(root.left, is(not(nullValue())) );
        assertThat(root.right, is(not(nullValue())) );

            Huffman.Node internalLeft = root.left;
            assertThat(internalLeft.freq, is(8));
            assertThat(root.value, is((byte) 0));
            assertThat(root.left, is(not(nullValue())) );
            assertThat(root.right, is(not(nullValue())) );

                Huffman.Node internalLeftLeft = internalLeft.left;
                assertThat(internalLeftLeft.freq, is(3));
                assertThat(internalLeftLeft.value, is((byte) 0));
                assertThat(internalLeftLeft.left, is(not(nullValue())) );
                assertThat(internalLeftLeft.right, is(not(nullValue())) );

                    Huffman.Node leafB = internalLeftLeft.left;
                    assertThat(leafB.freq, is(1));
                    assertThat(leafB.value, is((byte) 66));
                    assertThat(leafB.isLeaf(), is(true));

                    Huffman.Node leafD = internalLeftLeft.right;
                    assertThat(leafD.freq, is(2));
                    assertThat(leafD.value, is((byte) 68));
                    assertThat(leafD.isLeaf(), is(true));

                Huffman.Node leafE = internalLeft.right;
                assertThat(leafE.freq, is(5));
                assertThat(leafE.value, is((byte) 69));
                assertThat(leafE.isLeaf(), is(true));

            Huffman.Node internalRight = root.right;
            assertThat(internalRight.freq, is(12));
            assertThat(root.value, is((byte) 0));
            assertThat(root.left, is(not(nullValue())) );
            assertThat(root.right, is(not(nullValue())) );

                Huffman.Node leafA = internalRight.left;
                assertThat(leafA.freq, is(6));
                assertThat(leafA.value, is((byte) 65));
                assertThat(leafA.isLeaf(), is(true));

                Huffman.Node leafC = internalRight.right;
                assertThat(leafC.freq, is(6));
                assertThat(leafC.value, is((byte) 67));
                assertThat(leafC.isLeaf(), is(true));
    }

    @Test
    void compress_ShouldBeSmallerThanOriginal() throws Exception {
        loadInputOutput("/text/huffmanWiki.txt", "/res/huffmanWiki.huffman");
        huffman.compress(in, out);
        long lenSource = readAllBytes(Paths.get(RESOURCES_PATH, "text/huffmanWiki.txt")).length;
        long lenResult = readAllBytes(fs.getPath("/res/huffmanWiki.huffman")).length;

        assertThat(lenResult, is(lessThan(lenSource)));
    }

    @Test
    void headers_MustBeBit1AndBit0() {
        boolean[] headers = huffman.header();

        assertThat(headers, is( not( nullValue() ) ));
        assertThat(headers.length, is(2));
        assertThat(headers[0], is(true));
        assertThat(headers[1], is(true));
    }

    @Test
    void decompress_EmptyFileShouldFail() throws IOException {
        loadInputOutput("/text/huffmanWiki.txt", "/res/huffmanWiki.huffman");
        // Empty file has no header
        assertThrows(IllegalStateException.class, () -> huffman.decompress(in, out));
    }

    @Test
    void decompress_IncorrectHeaderShouldFail() {
        byte[] buf = new byte[] { 0x00 };
        in = new ByteArrayInputStream(buf);
        out = null;
        // Byte 1 is 00000000
        assertThrows(IllegalStateException.class, () -> huffman.decompress(in, out));
    }

    @Test
    void decompress_decompressJustCompressedFileMustBeEquals() throws IOException {
        loadInputOutput("/text/huffmanWiki.txt", "/res/huffmanWiki.huffman");
        huffman.compress(in, out);
        loadInputOutput("/res/huffmanWiki.huffman", "/res/huffmanWiki.txt.2");
        huffman.decompress(in, out);

        byte[] source = readAllBytes(fs.getPath("/text/huffmanWiki.txt"));
        byte[] result = readAllBytes(fs.getPath("/res/huffmanWiki.txt.2"));

        assertThat(Arrays.equals(source, result), is(true));

        loadInputOutput("/text/shortText.txt", "/res/shortText.huffman");
        huffman.compress(in, out);
        loadInputOutput("/res/shortText.huffman", "/res/shortText.txt.2");
        huffman.decompress(in, out);

        source = readAllBytes(fs.getPath("/text/shortText.txt"));
        result = readAllBytes(fs.getPath("/res/shortText.txt.2"));

        assertThat(Arrays.equals(source, result), is(true));
    }

    @Test
    void toByteArray_ExpectedBytes() throws IOException {
        byte[] buf = new byte[] { 0x01, 0x08, (byte) 0xA0, (byte) 0xED, (byte) 0xFF};
        try (
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(baos))
        {
            dos.write(buf, 0, buf.length);
            byte[] written = baos.toByteArray();
            assertThat(Arrays.equals(buf, written), is(true));
        }
    }

    @Test
    void decompress_ShouldBeLargerThanCompressed() throws Exception {
        loadInputOutput("/text/huffmanWiki.txt", "/res/huffmanWiki.huffman");
        huffman.compress(in, out);
        loadInputOutput("/res/huffmanWiki.huffman", "/res/huffmanWiki.txt.2");
        huffman.compress(in, out);

        long lenComp = readAllBytes(fs.getPath("/res/huffmanWiki.huffman")).length;
        long lenResult = readAllBytes(fs.getPath("/res/huffmanWiki.txt.2")).length;

        assertThat(lenResult, is(greaterThan(lenComp)));
    }


}