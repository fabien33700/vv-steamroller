package fr.istic.steam.roller.compress;

public class Usage {
    public static void main(String[] args) {

        args = new String[]{"-a", "huffman", "file1.txt"};
        Compressor.create(args).execute();

        args = new String[]{"-d", "-a", "huffman", "file1.txt.huffman"};
        Compressor.create(args).execute();

        args = new String[]{"-a", "lzw", "filetoencode.txt"};
        Compressor.create(args).execute();

        args = new String[]{"-d", "-a", "lzw", "filetoencode.txt.lzw"};
        Compressor.create(args).execute();
    }
}
