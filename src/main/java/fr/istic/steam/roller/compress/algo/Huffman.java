package fr.istic.steam.roller.compress.algo;

import fr.istic.steam.roller.compress.io.BinaryIn;
import fr.istic.steam.roller.compress.io.BinaryOut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Huffman implements Algorithm {

    private final static Logger LOG = LoggerFactory.getLogger(Huffman.class);

    static class Node {

        byte value;
        int freq;
        Node left, right;

        public Node(byte value, int freq, Node left, Node right) {
            this.value = value;
            this.freq = freq;
            this.left = left;
            this.right = right;
        }

        public Node(byte value, int freq) {
            this(value, freq, null, null);
        }

        public boolean isLeaf() {
            return left == null && right == null;
        }
    }

    @Override
    public boolean[] header() {
        return new boolean[] { true, true };
    }

    @Override
    public void compress(InputStream input, OutputStream output) throws IOException {
        // Convert InputStream to byte array
        byte[] data = toByteArray(input);

        if (data.length == 0) {
            LOG.warn("Input is empty, aborting ...");
            return;
        }

        // Utility wrapper to write bit-per-bit in an output stream
        final BinaryOut bo = new BinaryOut(output);

        // Calculate bytes frequencies
        Map<Byte, Integer> frequencies = getFrequencies(data);
        LOG.debug("Bytes frequencies : {}",  frequencies.entrySet().stream()
                .collect(Collectors.toMap(
                        e -> "0x" + Integer.toHexString(e.getKey()).toUpperCase(),
                        Map.Entry::getValue))
        );

        // Build Huffman tree
        Node tree = buildTree(frequencies);

        // Writing headers
        for (boolean bit : header())
            bo.write(bit);

        bo.write(data.length);

        // Encode huffman tree in the file
        encodeNode(tree, bo);

        // Build coding table from the tree
        Map<Byte, String> codingTable = getCodingTable(tree);

        // Writing encoded bytes
        for (byte b : data) {
            String symbol = codingTable.get(b);
            for (int i = 0; i < symbol.length(); i++) {
                boolean bit = symbol.charAt(i) == '1';
                bo.write(bit);
            }
        }

        bo.flush();
        bo.close();
    }

    @Override
    public void decompress(InputStream input, OutputStream output) throws IOException {
        BinaryIn bi = new BinaryIn(input);
        try (BufferedOutputStream bos = new BufferedOutputStream(output)) {
            boolean[] header = new boolean[] { bi.readBoolean(), bi.readBoolean() };
            if (!Arrays.equals(header, header())) {
                throw new IllegalStateException("The compressed stream is not encoded with huffman");
            }

            int size = bi.readInt();

            Node root = readTree(bi);
            Node current = root;
            int c = 0;

            while (!bi.isEmpty()) {
                boolean bit = bi.readBoolean();
                current = bit ? current.right : current.left;
                if (current == null) {
                    current = root;
                }
                if (current.isLeaf() && c++ < size) {
                    bos.write(current.value);
                    current = root;
                }
            }

            bos.flush();
        }
    }

    private void encodeNode(Node node, BinaryOut bo) {
        if (node == null)
            return;

        if (node.isLeaf()) {
            bo.write(true);
            bo.write(node.value);
        } else {
            bo.write(false);
            encodeNode(node.left, bo);
            encodeNode(node.right, bo);
        }
    }

    public static Node readTree(BinaryIn reader) {
        if (reader.readBoolean()) {
            return new Node(reader.readByte(), 0);
        } else {
            Node left = readTree(reader);
            Node right = readTree(reader);
            return new Node((byte) 0, 0, left, right);
        }
    }

    private static void encodeTable(Node root, String symbol, Map<Byte, String> codingTable) {
        if (root == null)
            return;

        if (root.isLeaf())
            codingTable.put(root.value, symbol);

        encodeTable(root.left, symbol + "0", codingTable);
        encodeTable(root.right, symbol + "1", codingTable);
    }

    public static Map<Byte, String> getCodingTable(Node root) {
        Map<Byte, String> codingTable = new HashMap<>();
        encodeTable(root, "", codingTable);
        return codingTable;
    }

    public static Node buildTree(Map<Byte, Integer> freq) {

        Comparator<Node> nodeComparator = Comparator
                .<Node>comparingInt(l -> l.freq)
                .thenComparingInt(l -> l.value);

        PriorityQueue<Node> queue = new PriorityQueue<>(nodeComparator);
        freq.forEach((c, f) -> queue.add(new Node(c, f)));

        while (queue.size() != 1) {
            Node left = queue.poll();
            Node right = queue.poll();

            int sum = (left != null ? left.freq : 0) + (right != null ? right.freq : 0);
            Node node = new Node((byte) 0, sum, left, right);
            queue.add(node);
        }

        return queue.peek();
    }

    public static Map<Byte, Integer> getFrequencies(byte[] data) {
        Map<Byte, Integer> freq = new HashMap<>();
        for (byte b : data) {
            freq.putIfAbsent(b, 0);
            freq.computeIfPresent(b, (k, v) -> v + 1);
        }
        return freq;
    }

    public static byte[] toByteArray(InputStream in) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];
        int len;

        while ((len = in.read(buffer)) != -1)
            os.write(buffer, 0, len);

        return os.toByteArray();
    }
}
