package fr.istic.steam.roller.compress;

import fr.istic.steam.roller.compress.algo.Algorithm;
import fr.istic.steam.roller.compress.algo.Huffman;
import fr.istic.steam.roller.compress.algo.LZW;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public final class Compressor {

    private static final Logger LOG = LoggerFactory.getLogger(Compressor.class);
    private static final Options CLI_OPTIONS = new Options();

    static {
        CLI_OPTIONS.addOption(Option
                .builder("d")
                .longOpt("decompress")
                .desc("Perform decompression of the input file")
                .build());

        CLI_OPTIONS.addOption(Option
                .builder("a")
                .required()
                .hasArg()
                .longOpt("algorithm")
                .desc("The compression algorithm to use (lzw, huffmann)")
                .build());

        CLI_OPTIONS.addOption(Option
                .builder("o")
                .hasArg()
                .longOpt("output")
                .desc("Specify a filename for the (de)compression output")
                .build());
    }

    private final boolean compress;

    private final Algorithm algorithm;

    private final String inFile;

    private final String outFile;

    Compressor(boolean compress, Algorithm algorithm, String inFile, String outFile) {
        this.compress = compress;
        this.algorithm = algorithm;
        this.inFile = inFile;
        this.outFile = outFile;
    }

    private static void showUsage() {
        final HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("steamroller [OPTIONS] [FILE]", CLI_OPTIONS, true);
        System.out.println();
    }

    public static Compressor create(String[] args) throws IllegalArgumentException {
        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine command = parser.parse(CLI_OPTIONS, args);

            boolean compress = !command.hasOption("d");

            final String[] remaining = command.getArgs();
            if (remaining.length == 0 || remaining[0] == null || remaining[0].trim().isEmpty())
                throw new IllegalArgumentException("You must provide a FILE as input for operation.");

            String inFile = remaining[0];
            String algoExt = command.getOptionValue("a");
            Algorithm algo;
            if ("lzw".equals(algoExt)) {
                algo = new LZW();
            } else if ("huffman".equals(algoExt)) {
                algo = new Huffman();
            } else {
                throw new IllegalArgumentException(algoExt + " is not a valid algorithm");
            }

            boolean hasOutFile = command.hasOption("o") && command.getOptionValue("o") != null;
            String outFile = hasOutFile ?
                    command.getOptionValue("o") :
                    (compress) ?
                            addExtension(inFile, algoExt) :
                            dropExtension(inFile);

            return new Compressor(compress, algo, inFile, outFile);
        } catch (ParseException | IllegalArgumentException ex) {
            System.err.println("ERROR : " + ex.getLocalizedMessage());
            showUsage();
            throw new IllegalArgumentException("Error parsing arguments : " + ex.getLocalizedMessage());
        }
    }

    private static String addExtension(String filename, String ext) {
        return filename + "." + ext;
    }

    private static String dropExtension(String filename) {
        return filename.substring(0, filename.lastIndexOf("."));
    }

    public void execute() {
        try (
                InputStream in = new FileInputStream(new File("files/" + inFile));
                OutputStream out = new FileOutputStream(new File("files/" + outFile))
        ) {
            String algoName = algorithm.getClass().getSimpleName();
            if (compress) {
                LOG.info("Compressing the file " + inFile + " using " + algoName);
                algorithm.compress(in, out);
            } else {
                LOG.info("Uncompressing the file " + inFile + " using " + algoName);

                algorithm.decompress(in, out);
            }
            LOG.info("Done. Output file is " + this.outFile);
        } catch (IOException ioe) {
            throw new UncheckedIOException("An I/O operation failed during compressing", ioe);
        }
    }

    public boolean isCompress() {
        return compress;
    }

    public Algorithm getAlgorithm() {
        return algorithm;
    }

    public String getInFile() {
        return inFile;
    }

    public String getOutFile() {
        return outFile;
    }
}
