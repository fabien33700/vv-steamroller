package fr.istic.steam.roller.compress.algo;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class LZW implements Algorithm {
    private int NUMBER_OF_ASCII_CHARS = 256;
    private Map<String, Integer> encodeDictionary = new HashMap<>();
    private Map<Integer, String> decodeDictionary = new HashMap<>();

    @Override
    public boolean[] header() {
        return new boolean[] { true, false };
    }

    @Override
    public void compress(InputStream input, OutputStream output) throws IOException {
        for (int i = 0; i < NUMBER_OF_ASCII_CHARS; i++){
            decodeDictionary.put( i,"" + (char) i);
            encodeDictionary.put(""+(char) i, i);
        }
        encode(input, output);
    }

    private void encode(InputStream inputStream, OutputStream outputStream) {
        String w = ""; // chaîne de comparaison
        String c = ""; // caractère lu
        byte read_byte = 0;
        int count = NUMBER_OF_ASCII_CHARS;
        try {
            DataInputStream dataInputStream = new DataInputStream(inputStream);
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            w = ""+(char)dataInputStream.readByte();
            int ifOrelse = 0;
            while (true) {
                try {
                    read_byte = dataInputStream.readByte();
                    if(read_byte != -1) {
                        c = "" + (char) read_byte;
                        if (encodeDictionary.containsKey(w+c)) {
                            w = w + c;
                            ifOrelse=0;
                        } else {
                            encodeDictionary.put(w+c, count);
                            writeToOutputFile(dataOutputStream, encodeDictionary.get(w));
                            w = c;
                            count++;
                            ifOrelse=1;
                        }
                    }
                }
                catch (EOFException exception) {
                    System.err.println("EOFException catched at the end of encode Algo");
                    break;
                }
            }
            if(ifOrelse==0) writeToOutputFile(dataOutputStream, encodeDictionary.get(w));
            dataInputStream.close();
            dataOutputStream.close();
        } catch (IOException exception) {
            System.err.println("IOException catched at the end of encode function:");
            exception.printStackTrace();
        }
    }

    private void writeToOutputFile(DataOutputStream dataOutputStream, int index) throws IOException {
        dataOutputStream.writeShort(index);
    }


    @Override
    public void decompress(InputStream input, OutputStream output) throws IOException {
        for (int i = 0; i < NUMBER_OF_ASCII_CHARS; i++){
            decodeDictionary.put( i,"" + (char) i);
        }
        decode(input, output);
    }

    private void decode(InputStream inputStream, OutputStream outputStream) {
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
        int step;
        int c;
        String w;
        String entree;
        int count = 256;
        try{
            step = dataInputStream.readByte();
            c = dataInputStream.readByte();

            if(step == 1){
                c += 256;
            }

            dataOutputStream.writeBytes(decodeDictionary.get(c));
            w = decodeDictionary.get(c);

            while(true){
                step = dataInputStream.readByte();
                c = dataInputStream.readByte();
                if(step != -1) {
                    if (step == 1) {
                        c += 256;
                    }

                    if (decodeDictionary.containsKey(c)) {
                        entree = decodeDictionary.get(c);
                    } else {
                        entree = w;
                        entree = entree + entree.substring(0,1);

                    }

                    dataOutputStream.writeBytes(entree);

                    decodeDictionary.put(count, w + entree.substring(0, 1));

                    count++;
                    w = entree;

                }
            }
        }
        catch (IOException exception){
            System.err.println("IOException catched at the end of decode function");
        }

        try{
            dataInputStream.close();
            dataOutputStream.close();
        }catch (IOException exception){
            System.err.println("IOException catched close stream :");
            exception.printStackTrace();
        }
    }

    public Map<String, Integer> getEncodeDictionary() {
        return encodeDictionary;
    }

    public Map<Integer, String> getDecodeDictionary() {
        return decodeDictionary;
    }

    public int getNUMBER_OF_ASCII_CHARS() {
        return NUMBER_OF_ASCII_CHARS;
    }
}
