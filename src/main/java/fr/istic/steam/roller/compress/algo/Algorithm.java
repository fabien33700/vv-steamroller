package fr.istic.steam.roller.compress.algo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface Algorithm {
    boolean[] header();
    void compress(final InputStream input, final OutputStream output) throws IOException;
    void decompress(final InputStream input, final OutputStream output) throws IOException;
}
